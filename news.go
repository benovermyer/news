package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"time"
)

type Response struct {
	Status       string
	TotalResults int
	Articles     []Article
}

type Article struct {
	Source      Source
	Author      string
	Title       string
	Description string
	Url         string
	UrlToImage  string
	PublishedAt string
}

type Source struct {
	Id   string
	Name string
}

var myClient = &http.Client{Timeout: 10 * time.Second}

func getJson(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func getTopHeadlines(apiKey string, options map[string]string) Response {
	url := "https://newsapi.org/v2/top-headlines?apiKey=" + apiKey

	for key, value := range options {
		url += "&" + key + "=" + value
	}

	response := new(Response)

	getJson(url, response)

	return *response
}

func printHeadlines(response Response) {
	for _, article := range response.Articles {
		fmt.Println("| " + article.Title)
		fmt.Println(article.Description)
		fmt.Println(article.Url)
		fmt.Println("+------")
	}
}

func main() {
	apiKey := flag.String("key", "", "Your News API key")
	country := flag.String("country", "", "Country code for country headlines")
	category := flag.String("category", "", "Category to get headlines for")
	sources := flag.String("sources", "", "A comma-delimited list of sources")

	flag.Parse()

	options := make(map[string]string)

	if *country != "" {
		options["country"] = *country
	}

	if *category != "" {
		options["category"] = *category
	}

	if *sources != "" {
		options["sources"] = *sources
	}

	headlines := getTopHeadlines(*apiKey, options)

	printHeadlines(headlines)
}
