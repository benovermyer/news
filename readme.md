# News

A command-line utility for fetching news headlines from the News API.

It has the following parameters:

`-key`: Your News API key. This is required.

`-country`: A two-character country code for the country's news you want. Ignored if you use `-sources`.

`-category`: A category for headlines. Ignored if you use `-sources`.

`-sources`: A comma-separated list of news sources or blogs to fetch news for. This overrides the above two parameters.
